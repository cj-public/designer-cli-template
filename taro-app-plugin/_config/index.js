/*
 * @Author: your name
 * @Date: 2021-05-20 13:57:03
 * @LastEditTime: 2021-06-05 15:12:04
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /run/config/index.js
 */
import configJson from '../package.json'

const config = {
  projectName: 'run',
  date: '2021-5-20',
  designWidth: 375,
  deviceRatio: {
    640: 2.34 / 2,
    750: 1,
    828: 1.81 / 2,
    375: 1 / 2
  },
  sourceRoot: 'src',
  outputRoot: 'dist',
  plugins: [
  ],
  defineConstants: {
  },
  copy: {
    patterns: [
      { from: 'node_modules/@xy/designlib/dist/designlib.min.js', to: 'dist/lib' },
      { from: 'node_modules/@xy/core/dist/core.min.js', to: 'dist/lib' }, 
      { from: 'node_modules/axios/dist/axios.min.js', to: 'dist/lib/' },
      { from: 'node_modules/vue/dist/vue.min.js', to: 'dist/lib/' },
      { from: 'node_modules/vuex/dist/vuex.min.js', to: 'dist/lib/' }
    ],
    options: {
    }
  },
  framework: 'vue',
  mini: {
    postcss: {
      pxtransform: {
        enable: true,
        config: {

        }
      },
      'postcss-px-scale': {
        'enable': true,
        'config': {
          'scale': 0.5, // 缩放为1/2
          'units': 'rpx',
          'includes': ['taro-ui']
        }
      },
      url: {
        enable: true,
        config: {
          limit: 1024 // 设定转换尺寸上限
        }
      },
      cssModules: {
        enable: false, // 默认为 false，如需使用 css modules 功能，则设为 true
        config: {
          namingPattern: 'module', // 转换模式，取值为 global/module
          generateScopedName: '[name]__[local]___[hash:base64:5]'
        }
      }
    }
  },
  h5: {
    esnextModules: ['taro-ui'],
    publicPath: '/',
    staticDirectory: 'static',
    postcss: {
      autoprefixer: {
        enable: true,
        config: {
        }
      },
      'postcss-px-scale': {
        'enable': true,
        'config': {
          'scale': 0.5, // 缩放为1/2
          'units': 'rem',
          'includes': ['taro-ui']
        }
      },
      cssModules: {
        enable: false, // 默认为 false，如需使用 css modules 功能，则设为 true
        config: {
          namingPattern: 'module', // 转换模式，取值为 global/module
          generateScopedName: '[name]__[local]___[hash:base64:5]'
        }
      }
    },
    webpackChain(chain, webpack) {
      chain.merge({
        devtool: 'source-map',
        externals: {
          'vue':'Vue',
          '@xy/core':'cjcore',
          'axios':'axios',
        }
      })
    }
  }
}

module.exports = function(merge) {
  if (process.env.NODE_ENV === 'development') {
    return merge({}, config, require('./dev'))
  }
  return merge({}, config, require('./prod'))
}
