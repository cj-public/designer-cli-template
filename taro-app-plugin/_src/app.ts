import Vue from 'vue'
import './app.scss'
import {openData,openEventObj,openStore} from "../example/open_data" 

Vue.prototype.openData = openData
Vue.prototype.openEventObj = openEventObj 
Vue.prototype.openStore = openStore
/** 全局Taro的样式引入 */
import "taro-ui-vue/dist/style/components/index.scss"

const App = {
  onShow (options) {
  },
  render(h) {
    // this.$slots.default 是将要会渲染的页面
    return h('block', this.$slots.default)
  }
}

export default App
