/** 按照Taro规范组织配置 */
export default {
  pages: [
    'pages/zbRoot/index',
    'pages/zbView/index',
  ],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black'
  }
}
