/*
 * @Author: your name
 * @Date: 2021-06-04 09:34:10
 * @LastEditTime: 2021-06-04 16:56:06
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /plugin-app1/src/modules/amdinModel.ts
 */
/** 自定义 store */
const state={
  num: 0
}

const mutations= {
  SET_NUM: (state, data) => {
      state.num = data
  }
}

const actions= {
  setNum({ commit }, data) {
    commit('SET_NUM', data)
  }
}

export const getters = {
  num: state => state.num
}

export default { 
  state,
  mutations,
  actions,
  getters
}

