import {
  createRouteByPathKey,
  Plugin,
  PluginAppListener,
  PluginRoute,
  PluginRouteDesc
} from '@xy/designlib'

import zbRoot from '../src/pages/zbRoot/index.vue'
import zbView from '../src/pages/zbView/index.vue'


/** 插件的生命周期方法 */
class MyPluginAppListener implements PluginAppListener{
  beforeCreate(): void {
  }
  created(): void {
  }
  beforeMount(): void {
  }
  mounted(): void {
  }
  beforeUpdate(): void {
  }
  updated(): void {
  }
  beforeDestroy(): void {
  }
  destroyed(): void {
  }
}


/** 应用插件入口类 */
export default class PluginMain extends Plugin {

  /** 构造函数 */
  constructor() {
    super()
    /** 生命周期管理 */
    this.attachListener(new MyPluginAppListener())
  }

  /** 插件名称 */
  registerName(): String {
    return 'pluginMain'
  }

  /** 配置插件路由 */
  configRoute(routes:PluginRoute): void {
    /** 注册应用拥有的所有视图 (最好注册的视图名称统一前缀 xxIndex，比如我这里就是zbXXXXX) */

    /** 第一个视图为应用启动的 根View 我们认为是插件的入口view */
    routes.register(new PluginRouteDesc(createRouteByPathKey('zbRoot'), zbRoot))
    /** 第二个视图 */
    routes.register(new PluginRouteDesc(createRouteByPathKey('zbView'), zbView))
  }
}
