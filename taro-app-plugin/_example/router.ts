/*
 * @Author: your name
 * @Date: 2021-06-02 13:55:57
 * @LastEditTime: 2021-06-05 14:16:41
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /run/src/utils/runtime/real/router.ts
 */
import { Route } from "@xy/designlib"
import Taro  from '@tarojs/taro'

export default class CjRouter {
    /**路由 */
    public static pushView(page:Route):void{
        //判断插件还是普通页面
        if(!page.pathKey){
            return
        } else {
            Taro.navigateTo({
              url: urlSpliceQuery(`/pages/${page.pathKey}/index`,page.props)
          })
        }
    }
    public static replaceView(page:Route):void{
        //判断插件还是普通页面
        if(!page.pathKey){
            return
        } else {
            Taro.redirectTo({
              url: urlSpliceQuery(`/pages/${page.pathKey}/index`,page.props)
          })
        }
    }
    public static backView(delta?:number):void{
        if(delta){
            let setp = Math.abs(delta)
            Taro.navigateBack({
                delta: setp
            })
        }else{
            Taro.navigateBack({
                delta: 1
            })
        }
    }
    public static switchTab(page:Route):void{ 
    }
}

function urlSpliceQuery(url?:string,params?:object) {
  if(!url) return '';
  if(!params) return url;
  if(params) {
      let queryArr = [];
      for (const key in params) {
          if (params.hasOwnProperty(key)) {
              queryArr.push(`${key}=${params[key]}`)
          }
      }
      if(url.indexOf('?') !== -1) {
          url =`${url}&${queryArr.join('&')}`
      } else {
          url =`${url}?${queryArr.join('&')}`
      }
  }
  return url;
}
