
import {  EventObjClass } from '@xy/core'
import Router from './router'  
import Vuex from 'vuex'
import Vue from 'vue'
Vue.use(Vuex)
 
// //////////////////////////////////////////////////////////////////////////////
const openEventObj = new EventObjClass()
/** 点击广告 */
openEventObj.on('E-AD-CLICK', (res) => {
  Log.log('广告点击', res)
})
/** 跳转到新页面或者tabbar切换 */
openEventObj.on('E_ROUTE_PUSH_VIEW', (res) => {
  Router.pushView(res)
}, null, { unique: true })
/** 返回上页 */
openEventObj.on('E_ROUTE_BACK', (res) => {
  Router.backView(res)
}, null, { unique: true })
/** 替换当前页面 */
openEventObj.on('E_ROUTE_REPLACE_VIEW', (res) => {
  Router.replaceView(res)
}, null, { unique: true })
/** tabbar切换 */
openEventObj.on('E_SWITCH_TABBAR', (res) => {
  Router.switchTab(res)
}, null, { unique: true })

/** 调起充值页面 */
openEventObj.on('E_ROUTE_RECHARGE', (res) => { 
}, null, { unique: true })

/** 插件调用登录 */
openEventObj.on('E_PLUGIN_LOGIN', (res) => {  
  return {aaa:111}
}, null, { unique: true })

/** 插件调用登出 */
openEventObj.on('E_PLUGIN_LOGIN_OUT', (res) => {
  return null
}, null, { unique: true }) 
 
// ////////////////////////////开放数据/////////////////////////////////////////////
/** 数据 */
class COpenData { 
  /** 获取插件配置 */
  getPluginConfig(id) {
    return {}
  }
  // 直播广告获取
  getLivePosters(locationPositions) {
    return []
  }
}
/** 开放数据 */
const openData = new COpenData()
/** stroe */
const openStore = new Vuex.Store({
  modules: {}
})


// ////////////////////////////导出/////////////////////////////////////////////
export {
  openData,
  openEventObj,
  openStore
}
