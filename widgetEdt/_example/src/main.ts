import Vue from 'Vue'
import HelloWorld from "./example.vue"

Vue.config.productionTip = false

new Vue({
  render: (h) => h(HelloWorld)
}).$mount('#app');





