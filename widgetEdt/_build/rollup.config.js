// rollup.config.js
import Package from '../package.json'
import minimist from 'minimist'
import sass from 'node-sass'

// rollup 模块转换插件
import vue from 'rollup-plugin-vue'
import babel from 'rollup-plugin-babel'
import typescript from 'rollup-plugin-typescript2'
import {nodeResolve} from '@rollup/plugin-node-resolve'
import commonjs from '@rollup/plugin-commonjs'

// rollup 优化插件
import filesize from 'rollup-plugin-filesize'
import cleaner from 'rollup-plugin-cleaner'
import replace from 'rollup-plugin-replace'
import {terser} from 'rollup-plugin-terser'

// rollup css插件
import postcss from 'rollup-plugin-postcss'

// rollup 资源插件
import svg from 'rollup-plugin-svg'
import json from 'rollup-plugin-json'

const ENV = process.env.NODE_ENV // node环境
const isProductionEnv = ENV === 'production'
const argv = minimist(process.argv.slice(2))

console.log(`当前的Node环境: ${ENV}`)

// eslint-disable-next-line no-unused-vars
const processSass = function(context, payload) {
  return new Promise((resolve, reject) => {
    sass.render(
      {
        file: context
      },
      function(err, result) {
        console.log(result)
        if (!err) {
          resolve(result)
        } else {
          console.log(err)
          reject(err)
        }
      }
    )
  })
}

const config = [
  {
    input: 'src/index.js',
    output: {
      name: '{{projectName}}',
      exports: 'named',
      sourcemap: false,
      globals: {
        'vue': 'Vue', 
        '@xy/designlib': 'designlib',
        '@xy/core': 'cjcore'
      }
    },

    // 排除第三方库不参与打包,外部用cdn载入
    external: isProductionEnv ? ['vue', '@xy/designlib', '@xy/core'] : [],
    plugins: [
      cleaner({
        targets: ['./dist']
      }),
      replace({
        'process.env.NODE_ENV': JSON.stringify('development'),
        'process.env.VUE_ENV': JSON.stringify('browser'),
        widgetVersion: Package.version
      }),
      nodeResolve({browser: true, preferBuiltins: true}),
      commonjs({
        include: 'node_modules/**'
      }),
      postcss({
        extract: 'src/style.css',
        // extract: true,
        minimize: isProductionEnv,
        extensions: ['css', 'scss', 'sass', 'less'],
        process: processSass
      }),
      json(),
      svg(),
      vue({
        css: true,
        compileTemplate: true,
        style: {
          postcssPlugins: [require('autoprefixer')]
        }
      }),
      typescript(),
      babel({
        runtimeHelpers: true,
        extensions: ['.js', '.jsx', '.es6', '.es', '.mjs', '.vue'],
        exclude: 'node_modules/**'
      }),
      filesize()
    ]
  }
]

// Only minify browser (iife) version
if (argv.format === 'iife') {
  config[0].plugins.push(terser())
}

export default config
