# {{projectName}}

组件

## Table of Contents

- [Introduction](#introduction)
- [Features](#features)
- [Install](#install)
- [Usage](#usage)
- [Links](#links)
- [Contributing](#contributing)
- [Contributors](#contributors)
- [License](#license)

{{projectName}}组件

## Features

[⬆ Back to Top](#table-of-contents)

## Install

- npm install @xy/{{projectName}}
- import {{projectName}} from {{projectName}}

[⬆ Back to Top](#table-of-contents)

## Contributors

[⬆ Back to Top](#table-of-contents)

## Feature

[⬆ Back to Top](#table-of-contents)
