module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint',
    ecmaFeatures: {
      legacyDecorators: true
    }
  },
  extends: [
    'plugin:vue/essential',
    '@vue/airbnb',
    '@vue/typescript/recommended',
  ],
  plugins: ['vue', 'prettier', 'jest'],
  rules: {
    'no-console': 'off',
    'no-debugger': 'error',
    'prettier/prettier': 'error',
    semi: [0]
  }
}
