import Vue from 'vue'
import HelloWorld from "./example.vue"

import { applyPolyfills, defineCustomElements } from '@tarojs/components/loader';
import "@tarojs/components/dist-h5/vue";

// 解决在非 taro 项目中使用 tarojs/component 组件导致的web component定义失效问题
applyPolyfills().then(() => {
	defineCustomElements(window);
});

Vue.config.productionTip = false

new Vue({
  render: (h) => h(HelloWorld)
}).$mount('#app');





