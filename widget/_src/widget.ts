import {
  WidgetBase,
  WidgetData,
  WidgetSetting,
  FillStyle,
  FontStyle,
  RegWidgetMethod,
  createRegWidgetMethod,
  createProp,
  PanelEditGrp,
  PanelEditItem,
  WidgetEvent,
  IWidgetEdt,
  WidgetLayoutMode,
  _WidgetLayoutMode,
  ImageRes,
  Color,
  _FillType,
  RadioItem
} from '@xy/designlib'

import cjbuttonjson from './data.json'
import {ButtonProps} from '@tarojs/components/types/Button'
import {CommonEvent} from '@tarojs/components'

/**
 * 组件的数据定义
 * @export
 * @interface {{className}}Data
 * @extends {WidgetData}
 */
export interface {{className}}Data extends WidgetData {
  label: string
}

/**
 * 按钮组件的配置定义
 * @export
 * @interface {{className}}Setting
 * @extends {WidgetSetting}
 */
export interface {{className}}Setting extends WidgetSetting {
  foreground: Color
  fillStyle: FillStyle
  fontStyle: FontStyle
  icon: ImageRes
  plain: Boolean
  size: ButtonProps.size
  type: ButtonProps.type
  disabled: Boolean
}

/**
 * 组件类
 * @description 组件的方法是可以加入到事件框架中去的方法我们约定采用widgetXXXX命名方式
 * @class {{className}}
 * @extends {WidgetBase}
 */
export class {{className}}Widget extends WidgetBase {
  /** 是否加载状态 */
  private isloading: Boolean = false
  /** 按钮的开启状态 */
  private disabled: Boolean = false

  /** 组件的构造函数 指定组件的类名 */
  constructor() {
    super('{{projectName}}')
  }

  /** 定义组件的布局类型 */
  public layoutModeDefine(): WidgetLayoutMode {
    return _WidgetLayoutMode.Flow
  }

  /** 定义配置字段 */
  public settingPropDefine(): {{className}}Setting {
    return {
      version: cjbuttonjson.setting.version,
      foreground: cjbuttonjson.setting.foreground,
      fillStyle: cjbuttonjson.setting.fillStyle,
      fontStyle: cjbuttonjson.setting.fontStyle,
      icon: cjbuttonjson.setting.icon,
      plain: cjbuttonjson.setting.plain,
      size: cjbuttonjson.setting.size,
      type: cjbuttonjson.setting.type,
      disabled: cjbuttonjson.setting.disabled
    }
  }

  /** 定义数据字段 */
  public dataPropDefine(): {{className}}Data {
    return {
      label: cjbuttonjson.data.label
    }
  }

  /** Get Set方法定义(可参与事件系统,我们约定采用widgetGetXXX,widgetSetXXX命名) */
  /** 按钮类型 */
  public widgetGetType(): ButtonProps.type {
    return this.getSettingV<ButtonProps.type>('type')
  }

  public widgetSetType(type: ButtonProps.type) {
    this.setSettingV<ButtonProps.type>('type', type)
  }

  //////////////////////
  /** 图标类型 */
  public widgetGetIcon(): String {
    return this.getSettingV<String>('icon')
  }

  public widgetSetIcon(iconname: String) {
    this.setSettingV<String>('icon', iconname)
  }

  //////////////////////
  /** 按钮标题 */
  public widgetGetLabel(): String {
    return this.getSettingV<String>('label')
  }
  public widgetSetLabel(label: string): void {
    this.setDataV<string>('label', label)
  }

  //////////////////////
  /** 背景填充 */
  public widgetGetFillStyle(): FillStyle {
    return this.getSettingV<FillStyle>('fillStyle')
  }
  public widgetSetFillStyle(fileStyle: FillStyle): void {
    this.setSettingV<FillStyle>('fillStyle', fileStyle)
  }

  //////////////////////
  /** 字体风格 */
  public widgetGetFontStyle(): FontStyle {
    return this.getSettingV<FontStyle>('fontStyle')
  }
  public widgetSetFontStyle(fontStyle: FontStyle): void {
    this.setSettingV<FontStyle>('fontStyle', fontStyle)
  }

  //////////////////////
  /** 设置加载状态 */
  public widgetGetLoading(): Boolean {
    return this.isloading
  }
  public widgetSetLoading(isloading: Boolean): void {
    this.isloading = isloading
  }

  //////////////////////
  /** 是否平面风格 */
  public widgetGetPlain(): Boolean {
    return this.getSettingV<Boolean>('plain')
  }
  public widgetSetPlain(isplain: Boolean): void {
    this.setSettingV<Boolean>('plain', isplain)
  }

  //////////////////////
  /** 按钮是否开启 */
  public widgetGetDisabled(): Boolean {
    return this.getSettingV<Boolean>('disabled')
  }
  public widgetSetDisabled(disabled: Boolean): void {
    this.setSettingV<Boolean>('disabled', disabled)
  }

  //////////////////////
  /** 获取前景色 */
  public widgetGetForeground(): Color {
    return this.getSettingV<Color>('foreground')
  }
  public widgetSetForeground(clr: Color): void {
    return this.setSettingV<Color>('foreground', clr)
  }

  //////////////////////
  /** 按钮尺寸 */
  public widgetGetSize(): ButtonProps.size {
    return this.getSettingV<ButtonProps.size>('size')
  }
  public widgetSetSize(size: ButtonProps.size): void {
    return this.setSettingV<ButtonProps.size>('size', size)
  }

  /** 按钮点击消息回调 */
  public handelOnClick(event: CommonEvent): void {
    if (!this.disabled) {
      console.log('我被按下了')
    }
  }
}

/**
 * (编辑状态)
 * @export
 * @class {{className}}Edit
 * @extends {WidgetEdtBase<{{className}}Widget>}
 */
export default class {{className}}Edit extends {{className}}Widget implements IWidgetEdt {
  /** 注册中文名称 */
  registerName(): string {
    return '按钮'
  }

  registerGetMethod(): RegWidgetMethod[] {
    return []
  }
  registerSetMethod(): RegWidgetMethod[] {
    return []
  }
  
  /** 注册组件方法 */
  registerMethod(): RegWidgetMethod[] {
    return [
      createRegWidgetMethod(
        '标题',
        'widgetSetLabel',
        [createProp('varName', 'string', '标题', '字体标题')],
        null
      ),
      createRegWidgetMethod(
        '背景色',
        'widgetSetBKColor',
        [createProp('varName', 'FillStyle', '填充', '字体背景')],
        null
      ),
      createRegWidgetMethod(
        '字体',
        'widgetSetFont',
        [createProp('varName', 'FontStyle', '字体', '设置字体')],
        null
      )
    ]
  }

  /** 配置属性面板(基本) 编辑组件的格式请参考编辑组件文档定义 */
  registerBaseConfig(): Array<PanelEditItem | PanelEditGrp> {
    /** 单选按钮定义 */
    let radioItem: Array<RadioItem> = [
      {
        label: '偏小',
        value: 'mini',
        icon: ''
      },
      {
        label: '默认',
        value: 'default',
        icon: ''
      }
    ]

    return [

      PanelEditItem.create('背景', 'cjeditcolor', 'setting.fillStyle', this, [
        {
          type: _FillType.ColorFill,
          value: cjbuttonjson.setting.fillStyle.value
        },
        {
          type: _FillType.LinearGradient,
          value: {
            direction: 45,
            colorList: ['#FFFFFF', '#000000']
          }
        },
        {
          type: _FillType.ImageFill,
          value: {
            type: 'iconFont',
            options: 'contain',
            value: '',
            size: 'large'
          }
        }
      ]),
      PanelEditItem.create('尺寸', 'cjeditradio', 'setting.size', this, {
        items: radioItem
      })
    ]
  }

  /** 配置属性面板(扩展) */
  registerExConfig(): Array<PanelEditItem | PanelEditGrp> {
    return []
  }

  /** 配置属性面板(内容) */
  registerContentConfig(): Array<PanelEditItem | PanelEditGrp> {
    return []
  }

  /** 配置属性面板(事件源) */
  registerEventConfig(): Array<WidgetEvent> {
    return []
  }
}
