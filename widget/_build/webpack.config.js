// 通用模块
const Webpack = require('webpack')
const path = require('path')
const packageConfig = require('../package.json')

// webpack 模块打包插件
const HtmlWebpackPlugin = require('html-webpack-plugin') // 打包html
const MiniCssExtractPlugin = require('mini-css-extract-plugin') // 打包css
// webpack loader
const VueLoaderPlugin = require('vue-loader/lib/plugin') // 打包vue
// webpack 优化
const {CleanWebpackPlugin} = require('clean-webpack-plugin') // 清除dist目录


const demoMainJsPath = path.resolve(__dirname, '../example/src/main.ts')
const demoMainOutputPath = path.resolve(__dirname, '../example/dist')
const templatePath = path.resolve(__dirname, '../example/public/index.html')
const templateOutputPath = path.resolve(__dirname, '../example/dist/index.html')

console.log(`入口文件路径: ${demoMainJsPath}`)
console.log(`打包输出文件路径: ${demoMainOutputPath}`)
console.log(`html模板路径: ${templatePath}`)
console.log(`html模板生成路径: ${templateOutputPath}`)
console.log(`当前的Node开发环境: ${process.env.NODE_ENV}`)

module.exports = {
  // 项目名称
  name: packageConfig.name,
  // 打包模式
  mode: 'development',
  // 入口文件，path.resolve()方法，可以结合我们给定的两个参数最后生成绝对路径，最终指向的就是我们的index.js文件
  entry: demoMainJsPath,
  externals: {
    vue: 'Vue',
    vant: 'vant',
    'element-ui': 'ELEMENT'
  },
  // 输出配置
  output: {
    // 输出路径
    path: demoMainOutputPath,
    publicPath: '',
    filename: '{{projectName}}.js'
  },

  resolve: {
    extensions: ['.ts', '.js']
  },

  module: {
    rules: [
      {
        test: /\.(png|gif|bmp|jpg)$/,
        use: 'url-loader?limit=5000&name=image/[hash:8]-[name].[ext]'
      },
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              minimize: true
            }
          }
        ]
      },
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {test: /\.(ttf|eot|svg|woff|woff2)$/, use: 'url-loader'},
      {
        test: /\.ts$/,
        exclude: /node_modules/,
        use: [
          'babel-loader',
          {
            loader: 'ts-loader',
            options: {appendTsxSuffixTo: [/\.vue$/]}
          }
        ]
      },
      /* 用babel来解析js文件并把es6的语法转换成浏览器认识的语法 */
      {
        test: /\.js$/,
        loader: 'babel-loader',
        /* 排除模块安装目录的文件 */
        exclude: /node_modules/
      }
    ]
  },

  plugins: [
    // 注入环境
    new Webpack.DefinePlugin({
      NODE_ENV: process.env.NODE_ENV
    }),
    new MiniCssExtractPlugin(),
    // 模块热替换
    new Webpack.HotModuleReplacementPlugin(),
    // 清理dist目录
    new CleanWebpackPlugin(),
    new VueLoaderPlugin(),
    // 生成html
    new HtmlWebpackPlugin({
      template: templatePath, // 指定要打包的html路径和文件名
      filename: './index.html', // 指定输出路径和文件名（相对js的路径,
      minify: {
        removeAttributeQuotes: false, // 删除标签属性的双引号
        collapseInlineTagWhitespace: false // 删除多余空格
      },
      hash: false // 增加hash，避免缓存
    })
  ],

  // webpack-dev-server 配置
  devServer: {
    port: 9999, // 端口号
    contentBase: 'example/dist/', // 服务默认指向文件夹，静态资源
    open: true // 自动打开浏览器
  },

  // 源代码调试
  devtool: '#cheap-module-eval-source-map'
}
