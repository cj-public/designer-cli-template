/*
 * @Author: your name
 * @Date: 2021-05-19 16:31:54
 * @LastEditTime: 2021-05-19 17:31:56
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /plugin-app1/example/src/router.js
 */

import Router from 'vue-router'
import viewA from "../../src/view/viewA.vue";
import viewB from "../../src/view/viewB.vue";
import viewC from "../../src/view/viewC.vue";
declare global {
    interface Router {
        rep: boolean,
        isBack:boolean
    }
}
const routList = [
    {
      path: '/',
      name: 'viewA',
      component: viewA,
      meta: { title: '页面A', noCache: false }
    },
    {
      path: '/viewB',
      name: 'viewB',
      component: viewB,
      meta: { title: '页面B', noCache: false }
    },
    {
      path: '/viewC',
      name: 'viewC',
      component: viewC,
      meta: { title: '页面C', noCache: false }
    }
]
 
export default routList