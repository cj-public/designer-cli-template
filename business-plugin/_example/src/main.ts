/*
 * @Author: your name
 * @Date: 2021-05-14 14:57:25
 * @LastEditTime: 2021-05-19 17:42:25
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /plugin-app1/example/src/main.ts
 */
import Vue from 'vue'
import Router from 'vue-router'
import routList from './router'
import HelloWorld from "./example.vue"
import { applyPolyfills, defineCustomElements } from '@tarojs/components/loader';
import "@tarojs/components/dist-h5/vue"; 



applyPolyfills().then(() => {
	defineCustomElements(window);
});
Vue.config.productionTip = false


var router = new Router({
  mode: 'history',
  routes: routList
})
new Vue({
  router,
  render: (h) => h(HelloWorld)
}).$mount('#app');





