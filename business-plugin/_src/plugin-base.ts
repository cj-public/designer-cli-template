/*
 * @Author: your name
 * @Date: 2021-05-14 14:57:25
 * @LastEditTime: 2021-05-19 19:17:53
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /plugin-app1/src/plugin-app1.ts
 */
import {  Plugin, PluginAppListener, PluginRoute, PluginRouteDesc, createRouteByPathKey } from "@xy/designlib";
import {Route, _RouteType, RouteType} from '@xy/designlib'
import viewA from './view/viewA.vue'
import viewB from './view/viewB.vue'
import viewC from './view/viewC.vue'
 
/** 我的插件 */
export default class PluginApp1 extends Plugin {

  /** 构造函数 */
  constructor() {
    super()
    this.addEventListener('userInfo',this.onUserInfoChange)
  }

  /** 插件名称 */
  registerName(): String {
    return '{{projectName}}'
  }

  /** 配置插件路由 */
  configRoute(routes: PluginRoute): void {
    routes.register(new PluginRouteDesc(createRouteByPathKey('ViewA'), viewA))
    routes.register(new PluginRouteDesc(createRouteByPathKey('viewB'), viewB))
    routes.register(new PluginRouteDesc(createRouteByPathKey('viewC'), viewC))
  }

  onUserInfoChange(value:any):void {
    console.log(`用户信息发生改变`)
  }
}
