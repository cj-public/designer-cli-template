# @xy/runtimecore

小程序运行时

## 目录

- [介绍](#introduction)
- [特性](#特性)
- [安装](#安装)
- [使用](#usage)
- [相关](#links)
- [贡献者](#贡献者)
- [许可](#license)

提供设计器的基础能力。

## 特性

- 纯 JS 库和具体技术栈分离

- 面向对象设计

- TypeScript 支持

- 插件式架构

- 支持 ES6 和 UMD 模块

[⬆ Back to Top](#table-of-contents)

## 安装

- npm install @xy/runtimecore

- yarn add @xy/runtimecore

[⬆ Back to Top](#table-of-contents)

## 贡献者

[⬆ Back to Top](#table-of-contents)
